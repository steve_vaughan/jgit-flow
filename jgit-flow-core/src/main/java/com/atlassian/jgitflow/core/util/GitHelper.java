package com.atlassian.jgitflow.core.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.atlassian.jgitflow.core.JGitFlowConstants;
import com.atlassian.jgitflow.core.exception.JGitFlowGitAPIException;
import com.atlassian.jgitflow.core.exception.JGitFlowIOException;
import com.atlassian.jgitflow.core.exception.LocalBranchMissingException;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ListBranchCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.lib.*;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.FileTreeIterator;
import org.eclipse.jgit.util.StringUtils;

/**
 * A helper class for common Git operations
 */
public class GitHelper
{
    /**
     * Checks to see if one branch is merged into another
     * @param git The git instance to use
     * @param branchName The name of the branch we're testing
     * @param baseName The name of the base branch to look for the merge
     * @return if the contents of branchName has been merged into baseName
     * @throws LocalBranchMissingException
     * @throws JGitFlowIOException
     * @throws JGitFlowGitAPIException
     */
    public static boolean isMergedInto(Git git, String branchName, String baseName) throws LocalBranchMissingException, JGitFlowIOException, JGitFlowGitAPIException
    {
        Repository repo = git.getRepository();
        RevWalk walk = new RevWalk(repo);
        walk.setRetainBody(false);

        try
        {
            ObjectId branch = repo.resolve(branchName);

            if (null == branch)
            {
                throw new LocalBranchMissingException(branchName + " does not exist");
            }

            RevCommit branchCommit = walk.parseCommit(branch);
            return isMergedInto(git, branchCommit, baseName);
        }
        catch (IOException e)
        {
            throw new JGitFlowIOException(e);
        }

    }

    /**
     * Checks to see if a specific commit is merged into a branch
     * @param git The git instance to use
     * @param commit The commit to test
     * @param branchName The name of the base branch to look for the merge
     * @return if the contents of commit has been merged into baseName
     * @throws JGitFlowGitAPIException
     * @throws JGitFlowIOException
     */
    public static boolean isMergedInto(Git git, RevCommit commit, String branchName) throws JGitFlowGitAPIException, JGitFlowIOException
    {
        Repository repo = git.getRepository();
        try
        {
            ObjectId base = repo.resolve(branchName);

            if (null == base)
            {
                return false;
            }

            Iterable<RevCommit> baseCommits = git.log().add(base).call();

            boolean merged = false;

            for (RevCommit entry : baseCommits)
            {
                if (entry.getId().equals(commit))
                {
                    merged = true;
                    break;
                }
                if (entry.getParentCount() > 1 && Arrays.asList(entry.getParents()).contains(commit))
                {
                    merged = true;
                    break;
                }
            }

            return merged;
        }
        catch (GitAPIException e)
        {
            throw new JGitFlowGitAPIException(e);
        }
        catch (IOException e)
        {
            throw new JGitFlowIOException(e);
        }
    }

    /**
     * Gets the latest commit for a branch
     * @param git The git instance to use
     * @param branchName The name of the branch to find the commit on
     * @return The latest commit for the branch
     * @throws JGitFlowIOException
     */
    public static RevCommit getLatestCommit(Git git, String branchName) throws JGitFlowIOException
    {
        RevWalk walk = null;
        try
        {
            ObjectId branch = git.getRepository().resolve(branchName);
            walk = new RevWalk(git.getRepository());
            walk.setRetainBody(true);

            return walk.parseCommit(branch);
        }
        catch (IOException e)
        {
            throw new JGitFlowIOException(e);
        }
        finally
        {
            if (null != walk)
            {
                walk.release();
            }
        }
    }

    /**
     * Checks to see if a local branch with the given name exists
     * @param git The git instance to use
     * @param branchName The name of the branch to look for
     * @return if the branch exists or not
     * @throws JGitFlowGitAPIException
     */
    public static boolean localBranchExists(Git git, String branchName) throws JGitFlowGitAPIException
    {
        boolean exists = false;

        if (StringUtils.isEmptyOrNull(branchName))
        {
            return exists;
        }

        try
        {

            List<Ref> refs = git.branchList().setListMode(null).call();
            for (Ref ref : refs)
            {
                String simpleName = ref.getName().substring(ref.getName().indexOf(Constants.R_HEADS) + Constants.R_HEADS.length());
                if (simpleName.equals(branchName))
                {
                    exists = true;
                    break;
                }
            }

            return exists;
        }
        catch (GitAPIException e)
        {
            throw new JGitFlowGitAPIException(e);
        }
    }

    /**
     * Checks to see if a remote branch with the given name exists
     * @param git The git instance to use
     * @param branch The name of the branch to look for
     * @return if the branch exists or not
     * @throws JGitFlowGitAPIException
     */
    public static boolean remoteBranchExists(Git git, final String branch) throws JGitFlowGitAPIException
    {
        boolean exists = false;

        if (StringUtils.isEmptyOrNull(branch))
        {
            return exists;
        }

        try
        {
            List<Ref> refs = git.branchList().setListMode(ListBranchCommand.ListMode.REMOTE).call();
            for (Ref ref : refs)
            {
                String simpleName = ref.getName().substring(ref.getName().indexOf(JGitFlowConstants.R_REMOTE_ORIGIN) + JGitFlowConstants.R_REMOTE_ORIGIN.length());
                if (simpleName.equals(branch))
                {
                    exists = true;
                    break;
                }
            }

            return exists;
        }
        catch (GitAPIException e)
        {
            throw new JGitFlowGitAPIException(e);
        }
    }

    /**
     * Gets a reference to a remote branch with the given name
     * @param git The git instance to use
     * @param branchName The name of the remote branch
     * @return A reference to the remote branch or null
     * @throws JGitFlowIOException
     */
    public static Ref getRemoteBranch(Git git, String branchName) throws JGitFlowIOException
    {
        try
        {
            final Map<String, Ref> refList = git.getRepository().getRefDatabase().getRefs(Constants.R_REMOTES);
            Ref remote = null;

            for (Map.Entry<String, Ref> entry : refList.entrySet())
            {
                int index = entry.getValue().getName().indexOf(JGitFlowConstants.R_REMOTE_ORIGIN);
                if (index < 0)
                {
                    continue;
                }

                String simpleName = entry.getValue().getName().substring(index + JGitFlowConstants.R_REMOTE_ORIGIN.length());
                if (simpleName.equals(branchName))
                {
                    remote = entry.getValue();
                    break;
                }
            }

            return remote;
        }
        catch (IOException e)
        {
            throw new JGitFlowIOException(e);
        }
    }

    /**
     * Gets a reference to a local branch with the given name
     * @param git The git instance to use
     * @param branchName The name of the remote branch
     * @return A reference to the local branch or null
     * @throws JGitFlowIOException
     */
    public static Ref getLocalBranch(Git git, String branchName) throws JGitFlowIOException
    {
        try
        {
            Ref ref2check = git.getRepository().getRef(branchName);
            Ref local = null;
            if (ref2check != null && ref2check.getName().startsWith(Constants.R_HEADS))
            {
                local = ref2check;
            }

            return local;
        }
        catch (IOException e)
        {
            throw new JGitFlowIOException(e);
        }
    }

    /**
     * Gets a list of branch references that begin with the given prefix
     * @param git The git instance to use
     * @param prefix The prefix to test for
     * @return A list of branch references matching the given prefix
     * @throws JGitFlowGitAPIException
     */
    public static List<Ref> listBranchesWithPrefix(Git git, String prefix) throws JGitFlowGitAPIException
    {
        List<Ref> branches = new ArrayList<Ref>();

        try
        {
            List<Ref> refs = git.branchList().setListMode(null).call();

            for (Ref ref : refs)
            {
                String simpleName = ref.getName().substring(ref.getName().indexOf(Constants.R_HEADS) + Constants.R_HEADS.length());
                if (simpleName.startsWith(prefix))
                {
                    branches.add(ref);
                }
            }

            return branches;
        }
        catch (GitAPIException e)
        {
            throw new JGitFlowGitAPIException(e);
        }
    }

    /**
     * Tests to see if a working folder is clean. e.g. all changes have been committed.
     * @param git The git instance to use
     * @return if the branch is clean or not
     * @throws JGitFlowIOException
     * @throws JGitFlowGitAPIException
     */
    public static boolean workingTreeIsClean(Git git) throws JGitFlowIOException, JGitFlowGitAPIException
    {
        try
        {
            List<DiffEntry> branchDiffs = git.diff().call();
            if (!branchDiffs.isEmpty())
            {
                return false;
            }

            branchDiffs = git.diff().setCached(true).call();

            if (!branchDiffs.isEmpty())
            {
                return false;
            }

            IndexDiff diffIndex = new IndexDiff(git.getRepository(), Constants.HEAD, new FileTreeIterator(git.getRepository()));

            if (diffIndex.diff())
            {
                return false;
            }

            return true;
        }
        catch (GitAPIException e)
        {
            throw new JGitFlowGitAPIException(e);
        }
        catch (IOException e)
        {
            throw new JGitFlowIOException(e);
        }
    }

    /**
     * Tests to see if a tag exists with the given name
     * @param git The git instance to use
     * @param tagName The name of the tag to test for
     * @return if the tag exists or not
     * @throws JGitFlowGitAPIException
     */
    public static boolean tagExists(Git git, final String tagName) throws JGitFlowGitAPIException
    {
        boolean exists = false;

        if (StringUtils.isEmptyOrNull(tagName))
        {
            return exists;
        }

        try
        {
            List<Ref> refs = git.tagList().call();
            for (Ref ref : refs)
            {
                String simpleName = ref.getName().substring(ref.getName().indexOf(Constants.R_TAGS) + Constants.R_TAGS.length());
                if (simpleName.equals(tagName))
                {
                    exists = true;
                    break;
                }
            }

            return exists;
        }
        catch (GitAPIException e)
        {
            throw new JGitFlowGitAPIException(e);
        }
    }

}
