package com.atlassian.jgitflow.core;

import com.atlassian.jgitflow.core.exception.*;
import com.atlassian.jgitflow.core.util.GitHelper;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.util.StringUtils;

import static com.atlassian.jgitflow.core.util.Preconditions.checkState;

/**
 * Start a hotfix.
 * <p>
 * This will create a new branch using the hotfix prefix and release name from the tip of develop
 * </p>
 * <p>
 * Examples (<code>flow</code> is a {@link JGitFlow} instance):
 * <p>
 * Start a feature:
 *
 * <pre>
 * flow.hotfixStart(&quot;1.0.1&quot;).call();
 * </pre>
 * <p>
 * Perform a fetch of develop before branching
 *
 * <pre>
 * flow.hotfixStart(&quot;1.0.1&quot;).setFetch(true).call();
 * </pre>
 */
public class HotfixStartCommand extends AbstractGitFlowCommand<Ref>
{
    private final String hotfixName;
    private boolean fetch;

    /**
     * Create a new hotfix start command instance.
     * <p></p>
     * An instance of this class is usually obtained by calling {@link JGitFlow#hotfixStart(String)}
     * @param hotfixName The name of the hotfix
     * @param git The git instance to use
     * @param gfConfig The GitFlowConfiguration to use
     */
    public HotfixStartCommand(String hotfixName, Git git, GitFlowConfiguration gfConfig)
    {
        super(git, gfConfig);

        checkState(!StringUtils.isEmptyOrNull(hotfixName));
        this.hotfixName = hotfixName;
        this.fetch = false;
    }

    /**
     * 
     * @return A reference to the new hotfix branch
     * @throws NotInitializedException
     * @throws JGitFlowGitAPIException
     * @throws HotfixBranchExistsException
     * @throws DirtyWorkingTreeException
     * @throws JGitFlowIOException
     * @throws LocalBranchExistsException
     * @throws TagExistsException
     * @throws BranchOutOfDateException
     */
    @Override
    public Ref call() throws NotInitializedException, JGitFlowGitAPIException, HotfixBranchExistsException, DirtyWorkingTreeException, JGitFlowIOException, LocalBranchExistsException, TagExistsException, BranchOutOfDateException
    {
        String prefixedHotfixName = gfConfig.getPrefixValue(JGitFlowConstants.PREFIXES.HOTFIX.configKey()) + hotfixName;

        requireGitFlowInitialized();
        requireNoExistingHotfixBranches();
        requireCleanWorkingTree();
        requireLocalBranchAbsent(prefixedHotfixName);

        try
        {
            if (fetch)
            {
                git.fetch().call();
            }

            requireTagAbsent(gfConfig.getPrefixValue(JGitFlowConstants.PREFIXES.VERSIONTAG.configKey()) + hotfixName);

            if (GitHelper.remoteBranchExists(git, gfConfig.getMaster()))
            {
                requireLocalBranchNotBehindRemote(gfConfig.getMaster());
            }

            RevCommit masterCommit = GitHelper.getLatestCommit(git, gfConfig.getMaster());

            return git.checkout()
                      .setName(prefixedHotfixName)
                      .setCreateBranch(true)
                      .setStartPoint(masterCommit)
                      .call();
        }
        catch (GitAPIException e)
        {
            throw new JGitFlowGitAPIException(e);
        }
    }

    /**
     * Set whether to perform a git fetch of the remote develop branch before branching
     * @param fetch
     *              <code>true</code> to do the fetch, <code>false</code>(default) otherwise
     * @return {@code this}
     */
    public HotfixStartCommand setFetch(boolean fetch)
    {
        this.fetch = fetch;
        return this;
    }
}
